/**
 * api请求按顺序传参
 * 写的时候麻烦，调用的时候也麻烦，顺序错请求就出问题了
 * 项目中还有十几个参数，也是这样一个个传的
 * 简单传个data对象不行吗？
 */

import axios from "axios";

export default api {
    addUser(name, phone, password, address, birthday) {
        return axios.post('/user', { name, phone, password, address, birthday })
    };
};